const axios = require('axios');
var express = require('express');
var app = express();

app.set('views', './views');
app.set('view engine', 'twig');

function getStationBoard(station, direction) {
  return axios
    .get('https://marudor.de/api/hafas/v1/DepStationBoard', {
      params: {
        profile: 'nahsh',
        station,
        direction,
      },
    })
    .then(r => r.data);
}

app.get('/', function(req, res) {
  Promise.all([
    getStationBoard('9027403', '9027326'),
    getStationBoard('9027434', '9027326'),
  ])
    .then(([departuresFromSolituede, departuresFromTwedterPlack]) => {
      const filteredDepartures = departuresFromSolituede.map(d => ({
        departure: new Date(d.departure.time),
        origin: 'Flensburg Solitüde/Gaststätte',
        id: d.train.number,
        name: d.train.name,
      }));

      departuresFromTwedterPlack.forEach(d => {
        if (
          !filteredDepartures.find(
            solituedeDeparture => solituedeDeparture.id === d.train.number
          )
        ) {
          filteredDepartures.push({
            departure: new Date(d.departure.time),
            origin: 'Flensburg Twedter Plack',
            id: d.train.number,
            name: d.train.name,
          });
        }
      });

      const sortedDepartures = filteredDepartures.sort((d1, d2) =>
        d1.departure > d2.departure ? 1 : -1
      );

      console.log(sortedDepartures);

      res.render('index', {
        info: sortedDepartures,
      });
    })
    .catch(e => {
      console.error(e);
      res.render('index', {
        info: [],
      });
    });
});

app.use(express.static('static'));

app.listen(8080, function() {
  console.log('Example app listening on port 8080!');
});
